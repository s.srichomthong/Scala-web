package models

import java.sql.Timestamp

case class Users(uid:String, email: String, name: String, password: String,
                 username: String, privacy: Boolean, regdate: Timestamp, imgurl:String)
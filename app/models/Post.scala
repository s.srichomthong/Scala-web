package models

import java.sql.Timestamp

import akka.http.scaladsl.model.DateTime

case class Post(id:String, content: String, dateTime: Timestamp, email:String, post_img_url:String)
case class PrePost(content: String)
case class FullPost(content:String, dateTime: DateTime, email:String, username:String, name:String)



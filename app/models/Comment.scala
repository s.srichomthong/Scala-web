package models

import java.sql.Timestamp

case class Comment(id:Int, content: String, dateTime: Timestamp, email:String)


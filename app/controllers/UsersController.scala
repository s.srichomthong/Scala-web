package controllers

import java.sql.Timestamp
import java.time
import javax.inject.Inject

import controllers.manager.{Generator, UsersManager}
import models.{Loginuser, RegUser, Users}
import play.api.data._
import play.api.mvc._

class UsersController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

  import controllers.formview.UsersForm._
  private val regSet = scala.collection.mutable.ArrayBuffer(RegUser("Users 1", "email1@email.com", "password", "password"))

  private val loginSet = scala.collection.mutable.ArrayBuffer(Loginuser("email.com", "password"))

  private val regUrl = routes.UsersController.register()
  private val loginUrl = routes.UsersController.login()

  ///////////////////////////////////////////////////////////////////////////////

  def reg = Action { implicit request: MessagesRequest[AnyContent] => Ok(views.html.index.register(regSet, form, regUrl))}

  def registered = Action { implicit request: MessagesRequest[AnyContent] =>Ok(views.html.index.registered())}

  def registerError = Action { implicit request: MessagesRequest[AnyContent] =>Ok(views.html.error.register_error())}

  def register = Action { implicit request: MessagesRequest[AnyContent] =>

    val errorFunction = { formWithErrors: Form[Data] =>
      BadRequest(views.html.index.register(regSet, formWithErrors, regUrl))
    }

    val successFunction = { data: Data =>
      val url = data.password match {
        case x if x == data.repassword => {
          val users = Users( Generator.generate(data.email.toUpperCase()) , data.email.toLowerCase(),
            data.name, Generator.generate(data.password), data.email.split("@")(0), false,
            Timestamp.valueOf(time.LocalDateTime.now()), "5b39c8b553c821e7cddc6da64b5bd2ee.jpg")
          UsersManager.register(users) match {
              case 1 => { routes.UsersController.registered() }
              case _ => { routes.UsersController.registerError() }
          }
        }
        case _ => {
          routes.UsersController.register()
        }
      }

      Redirect(url)
    }

    val formValidationResult = form.bindFromRequest
    formValidationResult.fold(errorFunction, successFunction)
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////

  def login = Action { implicit request: MessagesRequest[AnyContent] =>

    val errorLoginFunction = { formWithErrors: Form[loginData] =>
      BadRequest(views.html.index.login(loginSet, formWithErrors, loginUrl))
    }

    var mEmail = ""
    var mName = ""
    val successLoginFunction = { data: loginData =>
      var url = routes.ErrorHandlerController.catchall()
      isSingInDone(Loginuser(email = data.email, Generator.generate(data.password))) match {
        case Some(user) => {
          mEmail = user.email
          mName = user.name
          url = routes.HomeController.home()
        }
        case None => url = routes.UsersController.loginError()
      }
      if (mEmail.length > 3){
        Redirect(url).withSession("email" -> mEmail, "name" -> mName)
      } else Redirect(url)
    }

    val formValidationLoginResult = loginform.bindFromRequest
    formValidationLoginResult.fold(errorLoginFunction, successLoginFunction)
  }

  def isSingInDone(user: Loginuser): Option[Users] ={ UsersManager.loginNow(user) }

  def loginError = Action { implicit request: MessagesRequest[AnyContent] => Ok(views.html.error.login_error()) }
}
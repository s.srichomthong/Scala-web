package controllers.formview

object PostForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class PostData(content: String)

  val postForm = Form(
    mapping(
      "content" -> nonEmptyText
    )(PostData.apply)(PostData.unapply)
  )

}

package controllers.formview

object UsersForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(email: String,name: String,  password: String, repassword: String)
  case class loginData (email: String, password: String)
  case class mUser(id:Int, email:String, name:String, password:String)

  val form = Form(
    mapping(
      "email" -> nonEmptyText,
      "name" -> nonEmptyText,
      "password" -> nonEmptyText,
      "repassword" -> nonEmptyText
    )(Data.apply)(Data.unapply)
  )
  val loginform = Form(
    mapping(
      "email" -> nonEmptyText,
      "password" -> nonEmptyText
    )(loginData.apply)(loginData.unapply)
  )

  val mUserForm = Form(
    mapping(
      "id" -> number,
      "email" -> nonEmptyText,
      "name" -> nonEmptyText,
      "password" -> nonEmptyText
    )(mUser.apply)(mUser.unapply)
  )
}

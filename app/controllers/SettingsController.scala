package controllers

import javax.inject.Inject

import models._
import play.api.mvc._

class SettingsController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {
  private val postSet = scala.collection.mutable.ArrayBuffer( PrePost("content") )
  private val postUrl = routes.HomeController.home()

  //////////////////////////////////////////////////////////////////////////////////////////////////
  def terms = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.active.settings.terms())
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////
  def logout = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.index.index()).withNewSession
  }
}
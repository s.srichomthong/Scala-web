package controllers

import javax.inject.Inject

import play.api.mvc._

class ErrorHandlerController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

  def catchall() = Action { implicit request =>
    Ok(views.html.error.catchall())
  }

  def notFound(path: String) = Action { implicit request =>
    request.session.get("email").map { user =>
      if (user.length > 3) { Ok(views.html.error.not_found(user.concat("*" + path)))}
      else { Ok(views.html.error.not_found(path.toString)).withNewSession }
    }.getOrElse {
      Ok(views.html.error.not_found(path.toString)).withNewSession
    }
  }
}